# Hide Removed Messages

## Description
This is a userscript for use with the Tampermonkey browser extension. It automatically hides blocked or removed messages in Discord's web application.

## ⚠️ Deprecated
This script is outdated and may no longer work as intended due to changes in Discord's frontend.

## Features
- Automatically hides "removed message" placeholders in Discord's chat.
- Uses MutationObserver to handle dynamic content updates.

## Installation
1. Install the Tampermonkey browser extension.
2. Create a new userscript in Tampermonkey.
3. Copy and paste the provided script into the editor.
4. Save and enable the script.

## Compatibility
- Designed for the web version of Discord (`https://discordapp.com/channels/*`).
- May not work with newer versions of Discord's UI.

## Disclaimer
This script is provided "as is" without any warranty. It is a personal project and is no longer actively maintained.

## License
This script is provided under the MIT License.
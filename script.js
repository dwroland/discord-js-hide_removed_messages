// ==UserScript==
// @name         Hide removed messages
// @version      0.1
// @author       dwroland
// @match        https://discordapp.com/channels/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
var rem_attrib="display: none; border:none; margin: none;";
window.setTimeout(() => {
    document.querySelectorAll('div[class^=messageGroupBlocked]').forEach(div => div.setAttribute("style", rem_attrib));
    var stream = document.querySelector('div[class^=chat]').querySelector('div[class^=scroller]').querySelector('div[class^=scroller]');
    var observer = new MutationObserver(mutations => {
        mutations.forEach(mutation => {if (mutation.addedNodes.length>0 && mutation.addedNodes[0].querySelector('div[class^="messageGroupBlocked"]')!=null) mutation.addedNodes[0].querySelector('div[class^="messageGroupBlocked"]').setAttribute("style", rem_attrib)})
    });
    var config = {childList: true, subtree: true};
    observer.observe(stream, config);
},6000);
})();
